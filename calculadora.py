from tkinter import *

root=Tk()

miFrame=Frame(root)
miFrame.pack()

operator=""
operation=""
result = 0

valueDisplay = StringVar()

display=Entry(miFrame, textvariable=valueDisplay)
display.grid(row=1, column=1, padx=10, pady=10, columnspan=4)
display.config(background="#9F9F9F", fg="#333333", justify="right")

# functions
def setDisplay(value):
    global operator

    if operator != '':
        valueDisplay.set(value)
        operator=''
    else:
        valueDisplay.set(valueDisplay.get() + str(value))

def setOperation(value):
    global operator
    global operation
    global result

    operation=value
    

    if operator == 'igual':
        valueDisplay.set("0.0")
        operator=''
    elif operator != '':
        pass
    else:
        operator=value
        result+=float(valueDisplay.get())
        valueDisplay.set(result)
    

def setResult():
    global operator
    global result

    print('resultado')
    operator = 'igual'
    result+=float(valueDisplay.get())
    valueDisplay.set(result)
    result = 0
    print(operator)





# row1
button7=Button(miFrame, text="7", width=3, command=lambda:setDisplay(7))
button8=Button(miFrame, text="8", width=3, command=lambda:setDisplay(8))
button9=Button(miFrame, text="9", width=3, command=lambda:setDisplay(9))
buttonDiv=Button(miFrame, text="/", width=3, command=lambda:setOperation("division"))

button7.grid(row=2, column=1)
button8.grid(row=2, column=2)
button9.grid(row=2, column=3)
buttonDiv.grid(row=2, column=4)

# row 2
button4=Button(miFrame, text="4", width=3, command=lambda:setDisplay(4))
button5=Button(miFrame, text="5", width=3, command=lambda:setDisplay(5))
button6=Button(miFrame, text="6", width=3, command=lambda:setDisplay(6))
buttonMult=Button(miFrame, text="x", width=3, command=lambda:setOperation("producto"))

button4.grid(row=3, column=1)
button5.grid(row=3, column=2)
button6.grid(row=3, column=3)
buttonMult.grid(row=3, column=4)

# row 3
button1=Button(miFrame, text="1", width=3, command=lambda:setDisplay(1))
button2=Button(miFrame, text="2", width=3, command=lambda:setDisplay(2))
button3=Button(miFrame, text="3", width=3, command=lambda:setDisplay(3))
buttonRest=Button(miFrame, text="-", width=3, command=lambda:setOperation("resta"))

button1.grid(row=4, column=1)
button2.grid(row=4, column=2)
button3.grid(row=4, column=3)
buttonRest.grid(row=4, column=4)

#row 4
buttonComa=Button(miFrame, text=",", width=3, command=lambda:setDisplay('.'))
button0=Button(miFrame, text="0", width=3, command=lambda:setDisplay(0))
buttonIgual=Button(miFrame, text="=", width=3, command=lambda:setResult())
buttonSum=Button(miFrame, text="+", width=3, command=lambda:setOperation("suma"))

buttonComa.grid(row=5, column=1)
button0.grid(row=5, column=2)
buttonIgual.grid(row=5, column=3)
buttonSum.grid(row=5, column=4)


root.mainloop()